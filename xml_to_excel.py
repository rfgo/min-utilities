#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup as bs
import sys
import pandas as pd


def _item(element):
    values = {"tag": element.name}
    try:
        values.update(element.attrs)
        values.update({"value": element.text.strip()})
    except:
        pass
    return dict([("-".join(["item", k.strip()]), v) for k, v in values.iteritems()])

def _service(s):
    return dict([("service-" + k, v.strip()) for k, v in s.attrs.items()])

if __name__ == "__main__":
    fi = sys.argv[1]
    fo = sys.argv[2]
    xml = bs(open(fi).read())
    k = []
    for s in xml.select("service"):
        sv = _service(s)
        for c in s.children:
             if not isinstance(c, (str, unicode)):
                cv = _item(c)
                cv.update(sv)
                k.append(cv)
    df = pd.DataFrame(k)
    writer = pd.ExcelWriter(fo + '.xlsx')
    df.to_excel(writer, "Sheet1", index=False)
    writer.save()    
