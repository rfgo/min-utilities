import sys
import requests
from bs4 import BeautifulSoup as bs

URL_TO_FETCH = sys.argv[1]
r = requests.get(URL_TO_FETCH)
html = bs(r.text, 'html')
urls = [a.get('href') for a in html.select('a') if a.get('href') and a.get('href').endswith('.zip')]
for url in urls:
    print url
